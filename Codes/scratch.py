import pyautogui
# import mss
import numpy
import cv2
import time
from PIL import Image
from mss import mss
import PIL.ImageOps

screenWidth, screenHeight = pyautogui.size()
currentMouseY: int
currentMouseX, currentMouseY = pyautogui.position()
pyautogui.moveTo(100, 150)

print(screenWidth)
print(screenHeight)
print(f'{currentMouseX} {currentMouseY}')

# with mss.mss() as sct:
#     monitor = {'top': 40, 'left': 0, 'width': 800, 'height': 640}
#     img = numpy.array(sct.grab(monitor))
#     print(img)
#     print(sct.grab(monitor))


mon = {'top': 160, 'left': 160, 'width': 700, 'height': 700}
fps = 0
start_time = time.time()
sct = mss()
display_time = 1
template = numpy.array(cv2.imread('BlackDot.png',0))
template.astype(numpy.float32)
img_piece = cv2.cvtColor(template, cv2.COLOR_RGB2BGR)
c, w,h = img_piece.shape[::-1]
while 1:
    screenshot =sct.grab(mon)
    img = Image.frombytes('RGB', (screenshot.width, screenshot.height), screenshot.rgb)
    # img = PIL.ImageOps.invert(img)
    screenshot=numpy.array(screenshot)
    screenshot.astype(numpy.float32)
    meth = 'cv2.TM_CCOEFF'
    method = eval(meth)
    c, w ,h= screenshot.shape[::-1]
    res = cv2.matchTemplate(cv2.cvtColor(screenshot, cv2.COLOR_RGB2BGR), img_piece, method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    print(min_val, max_val, min_loc, max_loc)
    cv2.imshow('test', cv2.cvtColor(screenshot, cv2.COLOR_RGB2BGR))
    fps = fps + 1
    if time.time()-start_time >= display_time:
        start_time = time.time()
        print(fps)
        fps = 0
    if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break
