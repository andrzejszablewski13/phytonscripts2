import keyboard
import time
from FindObjectsClass import AnalyzeScreen,MovementControl
import concurrent.futures
import queue
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
import threading



fig, (ax2) = plt.subplots()
def ShowImg(img):
    # print(img)
    ax2.cla()
    # print(resme)
    # ax1.plot(), ax1.imshow(resme, cmap='gray')
    # plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
    ax2.plot(), ax2.imshow(img, cmap='gray')
    plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
    plt.suptitle(xScreen.methods[0])
    plt.pause(0.1)

if __name__ == "__main__":
    pipeline=queue.Queue(maxsize=10)
    showpipeline = queue.Queue(maxsize=10)
    xScreen = AnalyzeScreen(pipeline,showpipeline)
    y = MovementControl(pipeline)
    # print('test')
    with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.submit(y.LoopOfControl)
        executor.submit(xScreen.LoopOfFinding)
        while True:
            if not showpipeline.empty():
                message = showpipeline.get()
                img=message
                ShowImg(img)
            if keyboard.is_pressed("q"):
                cv.destroyAllWindows()
                break
