import math
import os
import cv2 as cv
import numpy as np
from mss import mss
from PIL import Image
import keyboard
from matplotlib import pyplot as plt
import time

# All the 6 self.methods for comparison in a list


fig, (ax2) = plt.subplots()


def showimg(img):
    # print(img)
    ax2.cla()
    # print(resme)
    # ax1.plot(), ax1.imshow(resme, cmap='gray')
    # plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
    ax2.plot(), ax2.imshow(img, cmap='gray')
    plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
    plt.suptitle('test view')
    plt.pause(0.1)


class ValuesOfPixels:
    pixposition = [[2, 0], [4, 0], [0, 1], [7, 1], [4, 2], [4, 3], [5, 3], [0, 4], [7, 4], [0, 6], [4, 6], [6, 6]]
    # Colors of pixels (from 0 to 9 and empty + in pil image cord are opposite)
    colorsofpixels = [
        [[[2, 0], [175, 176, 172]],
         [[4, 0], [175, 176, 172]],
         [[0, 1], [177, 178, 175]],
         [[7, 1], [194, 198, 194]],
         [[4, 2], [-1, -1, -1]],
         [[4, 3], [-1, -1, -1]],
         [[5, 3], [218, 219, 216]],
         [[0, 4], [211, 208, 208]],
         [[7, 4], [232, 232, 232]],
         [[0, 6], [166, 165, 161]],
         [[4, 6], [182, 183, 180]],
         [[6, 6], [182, 183, 180]],
         ],
        [[[2, 0], [-1, -1, -1]],
         [[4, 0], [175, 176, 172]],
         [[0, 1], [-1, -1, -1]],
         [[7, 1], [194, 198, 194]],
         [[4, 2], [216, 220, 216]],
         [[4, 3], [-1, -1, -1]],
         [[5, 3], [218, 219, 216]],
         [[0, 4], [-1, -1, -1]],
         [[7, 4], [232, 232, 232]],
         [[0, 6], [-1, -1, -1]],
         [[4, 6], [-1, -1, -1]],
         [[6, 6], [182, 183, 180]],
         ],
        [[[2, 0], [175, 176, 172]],
         [[4, 0], [175, 176, 172]],
         [[0, 1], [160, 158, 155]],
         [[7, 1], [194, 198, 194]],
         [[4, 2], [-1, -1, -1]],
         [[4, 3], [240, 244, 240]],
         [[5, 3], [240, 244, 240]],
         [[0, 4], [211, 208, 208]],
         [[7, 4], [-1, -1, -1]],
         [[0, 6], [166, 165, 161]],
         [[4, 6], [182, 183, 180]],
         [[6, 6], [182, 183, 180]],
         ],
        [[[2, 0], [175, 176, 172]],
         [[4, 0], [175, 176, 172]],
         [[0, 1], [160, 158, 155]],
         [[7, 1], [194, 198, 194]],
         [[4, 2], [-1, -1, -1]],
         [[4, 3], [240, 244, 240]],
         [[5, 3], [240, 244, 240]],
         [[0, 4], [-1, -1, -1]],
         [[7, 4], [232, 232, 232]],
         [[0, 6], [166, 165, 161]],
         [[4, 6], [182, 183, 180]],
         [[6, 6], [182, 183, 180]],
         ],
        [[[2, 0], [175, 176, 172]],
         [[4, 0], [-1, -1, -1]],
         [[0, 1], [177, 178, 175]],
         [[7, 1], [-1, -1, -1]],
         [[4, 2], [216, 220, 216]],
         [[4, 3], [240, 244, 240]],
         [[5, 3], [240, 244, 240]],
         [[0, 4], [211, 208, 208]],
         [[7, 4], [232, 232, 232]],
         [[0, 6], [-1, -1, -1]],
         [[4, 6], [182, 183, 180]],
         [[6, 6], [166, 165, 161]],
         ],
        [[[2, 0], [175, 176, 172]],
         [[4, 0], [175, 176, 172]],
         [[0, 1], [192, 187, 192]],
         [[7, 1], [175, 176, 172]],
         [[4, 2], [-1, -1, -1]],
         [[4, 3], [240, 244, 240]],
         [[5, 3], [240, 244, 240]],
         [[0, 4], [-1, -1, -1]],
         [[7, 4], [232, 232, 232]],
         [[0, 6], [166, 165, 163]],
         [[4, 6], [182, 183, 180]],
         [[6, 6], [182, 183, 180]],
         ],
        [[[2, 0], [175, 176, 172]],
         [[4, 0], [175, 176, 172]],
         [[0, 1], [177, 178, 175]],
         [[7, 1], [175, 176, 172]],
         [[4, 2], [-1, -1, -1]],
         [[4, 3], [240, 244, 240]],
         [[5, 3], [240, 244, 240]],
         [[0, 4], [211, 208, 208]],
         [[7, 4], [232, 232, 232]],
         [[0, 6], [181, 165, 161]],
         [[4, 6], [182, 183, 180]],
         [[6, 6], [182, 183, 180]],
         ],
        [[[2, 0], [175, 176, 172]],
         [[4, 0], [175, 176, 172]],
         [[0, 1], [160, 158, 155]],
         [[7, 1], [194, 198, 194]],
         [[4, 2], [-1, -1, -1]],
         [[4, 3], [240, 244, 240]],
         [[5, 3], [240, 244, 240]],
         [[0, 4], [-1, -1, -1]],
         [[7, 4], [-1, -1, -1]],
         [[0, 6], [-1, -1, -1]],
         [[4, 6], [182, 183, 180]],
         [[6, 6], [-1, -1, -1]],
         ],
        [[[2, 0], [175, 176, 172]],
         [[4, 0], [175, 176, 172]],
         [[0, 1], [177, 178, 175]],
         [[7, 1], [194, 198, 194]],
         [[4, 2], [-1, -1, -1]],
         [[4, 3], [240, 244, 240]],
         [[5, 3], [240, 244, 240]],
         [[0, 4], [211, 208, 208]],
         [[7, 4], [232, 232, 232]],
         [[0, 6], [166, 165, 161]],
         [[4, 6], [182, 183, 180]],
         [[6, 6], [182, 183, 180]],
         ],
        [[[2, 0], [175, 176, 172]],
         [[4, 0], [175, 176, 172]],
         [[0, 1], [177, 178, 175]],
         [[7, 1], [194, 198, 194]],
         [[4, 2], [-1, -1, -1]],
         [[4, 3], [240, 244, 240]],
         [[5, 3], [240, 244, 240]],
         [[0, 4], [-1, -1, -1]],
         [[7, 4], [232, 232, 232]],
         [[0, 6], [-1, -1, -1]],
         [[4, 6], [-1, -1, -1]],
         [[6, 6], [182, 183, 180]],
         ],
        [[[2, 0], [-1, -1, -1]],
         [[4, 0], [-1, -1, -1]],
         [[0, 1], [-1, -1, -1]],
         [[7, 1], [-1, -1, -1]],
         [[4, 2], [-1, -1, -1]],
         [[4, 3], [-1, -1, -1]],
         [[5, 3], [-1, -1, -1]],
         [[0, 4], [-1, -1, -1]],
         [[7, 4], [-1, -1, -1]],
         [[0, 6], [-1, -1, -1]],
         [[4, 6], [-1, -1, -1]],
         [[6, 6], [-1, -1, -1]],
         ],
    ]

class ScreenSA:

    def __init__(self):
        self.threetopleft=None
        self.cordinates=ValuesOfPixels()
        self.sct = mss()
        self.methods = ['cv.TM_CCOEFF', 'cv.TM_CCOEFF_NORMED', 'cv.TM_CCORR',
                        'cv.TM_CCORR_NORMED', 'cv.TM_SQDIFF', 'cv.TM_SQDIFF_NORMED']
        self.object = 'Numbers'
        self.tthreschold = 150
        three = cv.imread('three.png', cv.IMREAD_UNCHANGED)
        three = np.array(three)
        three.astype(np.float32)
        self.three = cv.cvtColor(three, cv.COLOR_BGR2RGB)
        neptunashooter = cv.imread('neptunashooter.png', cv.IMREAD_UNCHANGED)
        neptunashooter = np.array(neptunashooter)
        neptunashooter.astype(np.float32)
        self.neptunashooter = cv.cvtColor(neptunashooter, cv.COLOR_BGR2RGB)
        self.numbImgs = self.load_images_from_folder(self.object)
        self.zeroPos = (80, 0)
        self.mon = {'top': 240, 'left': 235, 'width': 652, 'height': 480}
        self.mondown=None
        self.number = 0
        self.hp = 3

    def load_images_from_folder(self, folder):
        images = []
        for filename in os.listdir(folder):
            img = cv.imread(os.path.join(folder, filename))
            if img is not None:
                img = self.recalculatesizesamller(np.array(img))
                # th, img = cv.threshold(img, self.tthreschold, 255, cv.THRESH_BINARY)
                images.append(img)
        return images

    def recalculatesizesamller(self, a, k=4):
        a = a[::k, ::k]
        img = Image.fromarray(a)
        img = np.array(img)
        return img

    def editfile(self, name):
        plate = cv.imread(name, cv.IMREAD_UNCHANGED)
        plate = self.recalculatesizesamller(plate)
        plate = np.array(plate)
        plate.astype(np.float32)
        plate = cv.cvtColor(plate, cv.COLOR_BGR2RGB)
        return plate

    def readscoreforreward(self, img, size, size2, hp=3):
        imgup = img[0:int(size[0] / 4), 2 * int(size[1] / 4) + 4:len(img[0]) - 1]
        # print(len(imgup),len(imgup[0]))
        th, imgup = cv.threshold(imgup, self.tthreschold, 255, cv.THRESH_BINARY)
        score = 0
        pos = self.zeroPos
        # smallimg = imgup[0:len(imgup) - 1, pos[0] - len(self.numbImgs[0][0]) - 2:pos[0]]
        # ShowImg(smallimg)
        # cv.rectangle(imgup, (self.zeroPos[0], 0), (self.zeroPos[0] + betwenvalue-2, len(imgup) - 1), 192, 1)
        betwenvalue = len(self.numbImgs[0][0]) + 2
        pos = (pos[0] - betwenvalue * 7, pos[1])
        for j in range(7):
            # print(len(imgup)-1,' ',pos[0]-len(self.numbImgs[0][0])-3,' ',pos[0]-1)
            # print(len(imgup),' ',len(imgup[0]))
            smallimg = imgup[0:len(imgup), pos[0]:pos[0] + betwenvalue]
            # cv.rectangle(imgup, (pos[0] + betwenvalue, 0), (pos[0], len(imgup)), 255, 1)
            pos = (pos[0] + betwenvalue, pos[1])
            testval = 0
            temp = 0
            testvalcom = False
            average = smallimg.mean(axis=0).mean(axis=0)
            average = math.fsum(average)
            for i in range(11):
                testnumber = cv.matchTemplate(smallimg, self.numbImgs[i], eval(self.methods[0]))
                min_val, max_val, min_loc, max_loc = cv.minMaxLoc(testnumber)
                if max_val > testval and average >= 100:
                    temp = i
                    testval = max_val
                    testvalcom = True

            if not temp == 10 and testvalcom:
                score = score * 10 + temp
                # average = smallimg.mean(axis=0).mean(axis=0)
                # average = math.fsum(average)
                # print(j,' ',temp,' ',average,' ',score,' ',testval)
        # print(score)
        # imgdown = img[len(img) - int(size2[0] / 4) - 2:len(img) - 1, len(img[0]) - int(size2[1] / 4) - 2:len(img[0])]
        # th, imgdown = cv.threshold(imgdown, self.tthreschold, 255, cv.THRESH_BINARY)
        # testval = 0
        # showimg(imgdown)
        # for i in range(10):
        #     testnumber = cv.matchTemplate(imgdown, self.numbImgs[i], eval(self.methods[1]))
        #     min_val, max_val, min_loc, max_loc = cv.minMaxLoc(testnumber)
        #     if max_val > testval:
        #         if abs(self.hp - i) <= 2:
        #             hp = i
        #             # print('testmaxval ',i,' ',max_val)
        #         testval = max_val
        imgdown=self.findwindow(self.mon2)
        screenshot = self.sct.grab(self.mon2)
        img = Image.frombytes('RGB', (screenshot.width, screenshot.height), screenshot.rgb)
        showimg(img)
        hp=self.hp
        for i in range(11):
            test=True
            for pix in self.cordinates.colorsofpixels[i]:
                if not pix[1][1]==-1 and not all(imgdown[pix[0][1]][pix[0][0]]==pix[1]):
                    test=False
                    print(f'false {imgdown[pix[0][1]][pix[0][0]]} {pix[1]}')
            if test:
                if not i==10:
                    hp=i
                    print(i)
                break
        print(hp)
        return score, hp

    def findwindow(self,mon):
        screenshot = self.sct.grab(mon)
        # img = Image.frombytes('RGB', (screenshot.width, screenshot.height), screenshot.rgb)
        img = np.array(screenshot)

        img = self.recalculatesizesamller(img)

        img.astype(np.float32)
        img = cv.cvtColor(img, cv.COLOR_BGR2RGB)
        return img

    def locatethewindow(self):
        test1, test2 = False, False
        val1, val2 = 0, 0
        while not test1 or not test2:
            screenshot = self.sct.grab(monitor={'top': 0, 'left': 0, 'width': 1080, 'height': 1080})
            img = Image.frombytes('RGB', (screenshot.width, screenshot.height), screenshot.rgb)
            img = np.array(img)
            img.astype(np.float32)
            img = cv.cvtColor(img, cv.COLOR_BGR2RGB)

            method = eval(self.methods[self.number])

            threeposition = cv.matchTemplate(img, self.three, method)
            min_val, max_val, min_loc, max_loc = cv.minMaxLoc(threeposition)
            # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
            if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
                top_left = min_loc
            else:
                top_left = max_loc
            bottom_right = (top_left[0] + self.three.shape[1], top_left[1] + self.three.shape[0])
            self.threetopleft=top_left
            if max_val >= 30000000:
                cv.rectangle(img, top_left, bottom_right, 128, 4)
                test2 = True
                val2 = (top_left[0] + self.three.shape[1], top_left[1] + self.three.shape[0])
            else:
                test2 = False

            neptunepos = cv.matchTemplate(img, self.neptunashooter, method)
            min_val, max_val, min_loc, max_loc = cv.minMaxLoc(neptunepos)
            # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
            if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
                top_left = min_loc
            else:
                top_left = max_loc
            bottom_right = (top_left[0] + self.neptunashooter.shape[1], top_left[1] + self.neptunashooter.shape[0])
            if max_val >= 50000000:
                cv.rectangle(img, top_left, bottom_right, 255, 4)
                test1 = True
                val1 = (top_left[0] + self.neptunashooter.shape[0], top_left[1])
            else:
                test1 = False
            showimg(img)
        self.mon = {'top': val1[0], 'left': val1[1], 'width': val2[0] - val1[1], 'height': val2[1] - val1[0]}
        self.mon2 = {'top':self.threetopleft[1]-1,'left':self.threetopleft[0]-1,
                   'width':32,'height':27 }
        print(self.mon2)
    def getobsandreward(self):
        img = self.findwindow(self.mon)
        score, hp = self.readscoreforreward(img, (len(self.neptunashooter), len(self.neptunashooter[0])),
                                            (len(self.three), len(self.three[0])))
        # showimg(img)
        self.hp = hp
        print(hp, ' ', score)
        return img, hp, score

    def resetgame(self):
        method = eval(self.methods[self.number])
        proportion = 7 / 8
        while True:
            time.sleep(0.1)
            keyboard.press('x')
            time.sleep(0.1)
            keyboard.press_and_release('x')
            img = self.findwindow(self.mon)
            img = img[int(len(img) * proportion):len(img), int(len(img[0]) * proportion):len(img[0])]
            threeposition = cv.matchTemplate(img, self.three, method)
            min_val, max_val, min_loc, max_loc = cv.minMaxLoc(threeposition)
            print(min_val, ' ', max_val)
            if max_val >= 2100000:
                break
