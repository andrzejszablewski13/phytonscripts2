from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# import os
# os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'#deactivate tf spamcom

import time
import tempfile
import os
import threading
import queue
import keyboard
import abc
import tensorflow as tf
import numpy as np
from ScreenSA import ScreenSA
import tf_agents
from tf_agents.environments import py_environment
from tf_agents.environments import tf_environment
from tf_agents.environments import tf_py_environment
from tf_agents.environments import utils
from tf_agents.specs import array_spec
from tf_agents.environments import wrappers
from tf_agents.environments import suite_gym
from tf_agents.trajectories import time_step as ts

from tf_agents.agents.dqn import dqn_agent
from tf_agents.drivers import dynamic_step_driver
from tf_agents.environments import suite_gym
from tf_agents.environments import tf_py_environment
from tf_agents.eval import metric_utils
from tf_agents.metrics import tf_metrics
from tf_agents.networks import q_network
from tf_agents.policies import random_tf_policy
from tf_agents.replay_buffers import tf_uniform_replay_buffer
from tf_agents.trajectories import trajectory
from tf_agents.utils import common

from tf_agents.specs import array_spec
from tf_agents.specs import tensor_spec
from tf_agents.networks import network

from tf_agents.policies import py_policy
from tf_agents.policies import random_py_policy
from tf_agents.policies import scripted_py_policy

from tf_agents.policies import tf_policy
from tf_agents.policies import random_tf_policy
from tf_agents.policies import actor_policy
from tf_agents.policies import q_policy
from tf_agents.policies import greedy_policy
from tf_agents.environments import random_py_environment
from tf_agents.environments import tf_py_environment
from tf_agents.networks import encoding_network
from tf_agents.networks import network
from tf_agents.networks import utils
from tf_agents.specs import array_spec
from tf_agents.utils import common as common_utils
from tf_agents.utils import nest_utils
from tf_agents.drivers import dynamic_episode_driver

tf.compat.v1.enable_v2_behavior()

tempdir = os.getenv("TEST_TMPDIR", tempfile.gettempdir())


class ControlSystem:
    def __init__(self,pipeline):
        self.pipeline=pipeline
        self.movevalue=0.01
    def ControlSystem(self):
        value1,value2,value3=0,0,0
        while True:
            if not self.pipeline.empty():
                message=self.pipeline.get()
                value1,value2,value3=message[0],message[1],message[2]
            if not value1==0 or not value2==0 or value3==0:
                keyboard.press('x')
            if value1>=self.movevalue:
                keyboard.press('h')
                keyboard.press_and_release('h')
            elif value1<=-self.movevalue:
                keyboard.press('f')
                keyboard.press_and_release('f')
            if value2>=self.movevalue:
                keyboard.press('t')
                keyboard.press_and_release('t')
            elif value2<=-self.movevalue:
                keyboard.press('g')
                keyboard.press_and_release('g')
            if value3>=self.movevalue or value3<=-self.movevalue:
                keyboard.press('i')
                keyboard.press_and_release('i')
            time.sleep(0.1)



class BotEnv(py_environment.PyEnvironment):

    def __init__(self):
        self.hpvalue = 2500
        self.ForcceEnd = False
        self.pipeline=queue.Queue(maxsize=5)
        self.movecontrol=ControlSystem(pipeline=self.pipeline)
        self.controlthread=threading.Thread(target=self.movecontrol.ControlSystem,daemon=True)
        self.controlthread.start()
        self.imgsource = ScreenSA()
        self.imgsource.locatethewindow()
        self.state, self.hp, self.score = self.imgsource.getobsandreward()
        self._action_spec = array_spec.BoundedArraySpec(
            shape=(3,), dtype=np.float32, minimum=-1, maximum=1, name='action')
        self._observation_spec = array_spec.BoundedArraySpec(
            shape=(1,len(self.state), len(self.state[0]), len(self.state[0][0]),), dtype=np.float32, name='observation')
        self._episode_ended = False

    def action_spec(self):
        return self._action_spec

    def observation_spec(self):
        return self._observation_spec

    def _reset(self):
        if not self.score==0:
            self.pipeline.put((0,0,0))
            print('testreset')
            self.hp = 3
            self.score = 0
            # input('whait for manual reset')
            self.imgsource.resetgame()
            self.state, self.hp, self.score = self.imgsource.getobsandreward()
        return ts.restart(np.array([self.state], dtype=np.float32))

    def _step(self, action):
        # print('teststep1',action)
        if self._episode_ended:
            # The last action ended the episode. Ignore the current action and start
            # a new episode.
            return self.reset()
        message=(action)
        # print('teststep2', message)
        self.pipeline.put(message)
        # Make sure episodes don't go on forever.
        if self.hp == 0 or self.ForcceEnd:
            self._episode_ended = True
        else:
            self.state, hp, score = self.imgsource.getobsandreward()
        print('teststep3',self.hp,' ',hp)

        if self._episode_ended or hp == 0:
            reward = self.hp * self.hpvalue + self.score
            return ts.termination(np.array([self.state], dtype=np.float32), reward)
        else:
            dif = self.hp - hp
            self.hp = hp
            dif2 = score - self.score
            self.score = score
            reward = dif * self.hpvalue + dif2
            return ts.transition(
                np.array([self.state], dtype=np.float32), reward=reward, discount=0.5)


tf_env = tf_py_environment.TFPyEnvironment(BotEnv())
print(tf_env.batch_size,'',tf_env.reward_spec())
print(isinstance(tf_env, tf_environment.TFEnvironment))
print("TimeStep Specs:", tf_env.time_step_spec())
print("Action Specs:", tf_env.action_spec())

my_random_tf_policy = random_tf_policy.RandomTFPolicy(
    action_spec=tf_env.action_spec(), time_step_spec=tf_env.time_step_spec())
observation = tf.ones(tf_env.time_step_spec().observation.shape)
time_step = ts.restart(observation)
action_step = my_random_tf_policy.action(time_step)

print('Action:')
print(action_step.action)
print(action_step[0][0])
print(action_step[0][0].numpy())

# wrapper1=wrappers.ActionDiscretizeWrapper(tf_env,5)
# print('Discretized Action Spec:', wrapper1.action_spec())
# # wrapper5=wrappers.FlattenObservationsWrapper(tf_env)
# # print('Discretized Action Spec:', wrapper5.action_spec())
# wrapper3=wrappers.ActionOffsetWrapper(tf_env)
# # print('Discretized Action Spec:', wrapper3.action_spec().shape,' ')
# wrapper4=wrappers.ActionClipWrapper(tf_env)
# print('Discretized Action Spec:', wrapper4.action_spec())
# wrapper2=wrappers.OneHotActionWrapper(tf_env)
# print('Discretized Action Spec:', wrapper2.action_spec())


# work to this moment
class ActorNetwork(network.Network):

    def __init__(self,
                 observation_spec,
                 action_spec,
                 preprocessing_layers=None,
                 preprocessing_combiner=None,
                 conv_layer_params=None,
                 fc_layer_params=(75, 40),
                 dropout_layer_params=None,
                 activation_fn=tf.keras.activations.relu,
                 enable_last_layer_zero_initializer=False,
                 name='ActorNetwork'):
        super(ActorNetwork, self).__init__(
            input_tensor_spec=observation_spec, state_spec=(), name=name)

        # For simplicity we will only support a single action float output.
        self._action_spec = action_spec
        flat_action_spec = tf.nest.flatten(action_spec)
        if len(flat_action_spec) > 1:
            raise ValueError('Only a single action is supported by this network')
        self._single_action_spec = flat_action_spec[0]
        if self._single_action_spec.dtype not in [tf.float32, tf.float64]:
            raise ValueError('Only float actions are supported by this network.')

        kernel_initializer = tf.keras.initializers.VarianceScaling(
            scale=1. / 3., mode='fan_in', distribution='uniform')
        self._encoder = encoding_network.EncodingNetwork(
            observation_spec,
            preprocessing_layers=preprocessing_layers,
            preprocessing_combiner=preprocessing_combiner,
            conv_layer_params=conv_layer_params,
            fc_layer_params=fc_layer_params,
            dropout_layer_params=dropout_layer_params,
            activation_fn=activation_fn,
            kernel_initializer=kernel_initializer,
            batch_squash=False)

        initializer = tf.keras.initializers.RandomUniform(
            minval=-0.003, maxval=0.003)

        self._action_projection_layer = tf.keras.layers.Dense(
            flat_action_spec[0].shape.num_elements(),
            activation=tf.keras.activations.tanh,
            kernel_initializer=initializer,
            name='action')

    def call(self, observations, step_type=(), network_state=()):
        outer_rank = nest_utils.get_outer_rank(observations, self.input_tensor_spec)
        # We use batch_squash here in case the observations have a time sequence
        # compoment.
        batch_squash = utils.BatchSquash(outer_rank)
        observations = tf.nest.map_structure(batch_squash.flatten, observations)

        state, network_state = self._encoder(
            observations, step_type=step_type, network_state=network_state)
        actions = self._action_projection_layer(state)
        actions = common_utils.scale_to_spec(actions, self._single_action_spec)
        actions = batch_squash.unflatten(actions)
        return tf.nest.pack_sequence_as(self._action_spec, [actions]), network_state


actor = ActorNetwork(tf_env.observation_spec(), tf_env.action_spec())
print('actor ready')
# way to add preprocessing
# preprocessing_layers = {
#     'image': tf.keras.models.Sequential([tf.keras.layers.Conv2D(8, 4),
#                                         tf.keras.layers.Flatten()]),
#     'vector': tf.keras.layers.Dense(5)
#     }
# preprocessing_combiner = tf.keras.layers.Concatenate(axis=-1)
# actor = ActorNetwork(tf_env.observation_spec(),
#                      tf_env.action_spec(),
#                      preprocessing_layers=preprocessing_layers,
#                      preprocessing_combiner=preprocessing_combiner)

# time_step = tf_env.reset()
# actor(time_step.observation, time_step.step_type)

my_actor_policy = actor_policy.ActorPolicy(
    time_step_spec=tf_env.time_step_spec(),
    action_spec=tf_env.action_spec(),
    actor_network=actor)
print('actor policy ready')
# #batch size-how many move to save
# batch_size = 1
# observations = tf.ones([1] + tf_env.time_step_spec().observation.shape.as_list())
#
# time_step = ts.restart(observations, batch_size)
#
# print(tf_env.current_time_step())
# action_step = my_actor_policy.action(time_step)
# print('Action:')
# print(action_step.action)
#
# distribution_step = my_actor_policy.distribution(time_step)
# print('Action distribution:')
# print(distribution_step.action)


# zamraża się na tym

# num_steps = 3
# transitions = []
# reward = 0
# policy_state=my_actor_policy.get_initial_state(batch_size=1)
# batch_size = 1
# observations = tf.ones([1] + tf_env.time_step_spec().observation.shape.as_list())
# # print('test')
# # print(tf_env.current_time_step())
# time_step = tf_env.current_time_step()
# # print(time_step)
# # time_step=None
# print('test1',policy_state)
# # print('test2',time_step)
# # print('test3',time_step[0])
# print(my_actor_policy.time_step_spec)
# for i in range(num_steps):
#   action = my_actor_policy.action(time_step,policy_state)
#   print('test4',action,' \n',action[0][0])
#   # applies the action and returns the new TimeStep.
#   next_time_step = tf_env.step(action)
#   print('test5', next_time_step)
#   transitions.append([time_step, action, next_time_step])
#   reward += next_time_step.reward
#   time_step = next_time_step
#
# np_transitions = tf.nest.map_structure(lambda x: x.numpy(), transitions)
# print('\n'.join(map(str, np_transitions)))
# print('Total reward:', reward.numpy())

num_episodes = tf_metrics.NumberOfEpisodes()
env_steps = tf_metrics.EnvironmentSteps()
observers = [num_episodes, env_steps]
driver = dynamic_episode_driver.DynamicEpisodeDriver(
    tf_env, my_actor_policy, observers, num_episodes=1)

# Initial driver.run will reset the environment and initialize the policy.
final_time_step, policy_state = driver.run()

print('final_time_step', final_time_step)
print('Number of Steps: ', env_steps.result().numpy())
print('Number of Episodes: ', num_episodes.result().numpy())
global_step = tf.compat.v1.train.get_or_create_global_step()
checkpoint_dir = os.path.join(tempdir, 'checkpoint')
train_checkpointer = common.Checkpointer(
    ckpt_dir=checkpoint_dir,
    max_to_keep=1,
    agent=actor,
    policy=my_actor_policy,
    global_step=global_step
)
# train_checkpointer.save(global_step)
for _ in range(5):
    time.sleep(5)
    final_time_step, policy_state = driver.run(time_step=final_time_step,policy_state=policy_state)
    print('final_time_step', final_time_step)
    print('Number of Steps: ', env_steps.result().numpy())
    print('Number of Episodes: ', num_episodes.result().numpy())
    # train_checkpointer.save(global_step)

