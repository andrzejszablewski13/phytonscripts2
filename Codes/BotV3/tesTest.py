import keyboard
import time

# while True:
#     time.sleep(1)
#     keyboard.press('x')
#     time.sleep(1)
#     keyboard.press_and_release('x')
# while True:
#     time.sleep(1)
#     keyboard.press('t')
#     time.sleep(1)
#     keyboard.press_and_release('t')
from PIL import Image
import numpy as np
import cv2 as cv
import os
path = 'Numbers2'

def recalculatesizesamller(a, k=4):
    a = a[::k, ::k]
    img = Image.fromarray(a)
    img = np.array(img)
    return img

def load_images_from_folder(folder):
        images = []
        for filename in os.listdir(folder):
            img = cv.imread(os.path.join(folder, filename))
            if img is not None:
                img =recalculatesizesamller(np.array(img))
                # cv.imwrite(os.path.join(path, filename), img)
                img=np.asarray(img)
                images.append(img)
        return images

img=load_images_from_folder('Numbers')
pixposition=[[2,0],
[4,0],
[0,1],
[7,1],
[4,2],
[4,3],
[5,3],
[0,4],
[7,4],
[0,6],
[4,6],
[6,6]]
f=open('pixvaluedata.txt',mode='w')
f.write(f'Pixposition \n')
f.write(f'{pixposition} \n')
f.write(f'Colors of pixels (from 0 to 9 and empty + in pil image cord are opposite)\n[')
for img in img:
    f.write(f'[')

    for pix in pixposition:
        if not all(img[pix[1]][pix[0]]==(24 , 0 , 0)):
            f.write(f'[{pix},[{img[pix[1]][pix[0]][0]},{img[pix[1]][pix[0]][1]},{img[pix[1]][pix[0]][2]}]], \n')
        else:
            f.write(f'[{pix},{[-1,-1,-1]}], \n')
    f.write(f'],\n')
f.write(']')

f.close()