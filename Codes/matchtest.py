import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
from mss import mss
from PIL import Image
import time
import keyboard
import pyautogui


sct = mss()
mon = {'top': 240, 'left': 235, 'width': 650, 'height': 480}
object = 'neptune.png'
fig, (ax1, ax2) = plt.subplots(1,2, clear='False')


def FindObject(object):
    screenshot = sct.grab(mon)
    img = Image.frombytes('RGB', (screenshot.width, screenshot.height), screenshot.rgb)
    img = np.array(img)
    img.astype(np.float32)
    img = cv.cvtColor(img, cv.COLOR_RGBA2RGB)
    method = eval(methods[number])
    # Apply template Matching
    res = cv.matchTemplate(img, template, method)
    min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
    # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
    if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
        top_left = min_loc
    else:
        top_left = max_loc
    bottom_right = (top_left[0] + w, top_left[1] + h)
    cv.rectangle(img, top_left, bottom_right, 255, 2)
    # find enemies and generate rectangle around them
    res = cv.matchTemplate(img, bluedoggo, cv.TM_CCOEFF_NORMED)
    we, he, ze = bluebullet.shape
    threshold = 0.8
    loc = np.where(res >= threshold)
    for pt in zip(*loc[::-1]):
        cv.rectangle(img, pt, (pt[0] + we, pt[1] + he), (0, 0, 255), 2)
    # find enemy bullets and generate rectangle around them
    res = cv.matchTemplate(img, bluebullet, cv.TM_CCOEFF_NORMED)
    wb, hb, zb = bluebullet.shape
    threshold = 0.8
    loc = np.where(res >= threshold)
    for pt in zip(*loc[::-1]):
        cv.rectangle(img, pt, (pt[0] + wb, pt[1] + hb), (0, 255, 128), 2)

    ax1.cla()
    ax2.cla()
    ax1.plot(), ax1.imshow(res, cmap='gray')
    plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
    ax2.plot(), ax2.imshow(img, cmap='gray')
    plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
    plt.suptitle(methods[number])
    plt.pause(0.01)

def EditFile(name):
    plate = cv.imread(name, cv.IMREAD_GRAYSCALE)
    plate = np.array(plate)
    plate.astype(np.float32)
    plate = cv.cvtColor(plate, cv.COLOR_RGBA2RGB)
    return plate

template=EditFile(object)
bluedoggo=EditFile('doggoblue.png')
bluebullet=EditFile('bulletblue.png')


w, h, z = template.shape
start_time = time.time()
fps=0
display_time = 1
number=0
# All the 6 methods for comparison in a list
methods = ['cv.TM_CCOEFF', 'cv.TM_CCOEFF_NORMED', 'cv.TM_CCORR',
               'cv.TM_CCORR_NORMED', 'cv.TM_SQDIFF', 'cv.TM_SQDIFF_NORMED']

while 1:
    fps = fps + 1
    if time.time() - start_time >= display_time:
        start_time = time.time()
        print(fps)
        fps = 0
    if keyboard.is_pressed("n"):
        number=number+1
        print("test ",number)
        if len(methods) <= number:
            number=0
            print('test2')
    FindObject(object)
    if keyboard.is_pressed("q"):
        cv.destroyAllWindows()
        print("test")
        break

# FindObject(object)
