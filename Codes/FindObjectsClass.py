import cv2 as cv
import numpy as np
from mss import mss
from PIL import Image
import time
import keyboard
import logging
import queue
import sys
import threading
import concurrent.futures



def RecalculateSizeSamller(a,k=4):
    # print(len(a), ' ', len(a[0]), ' ', len(a[0][0]))
    img = Image.new('RGB', [int(len(a[0])/k), int(len(a)/k)], 255)
    data = img.load()
    # with concurrent.futures.ThreadPoolExecutor() as executor:
    #     for x in range(int(len(a)/k)):
    #         args=(a,data,k,x)
    #         executor.submit(RecalculatedSizeSmallerThred,args)
    # for x in range(int(len(a) / k)):
    #     for y in range(int(len(a[0]) / k)):
    #         data[y, x] = (a[k * x][k * y][0], a[k * x][k * y][1], a[k * x][k * y][2])
    # f = open('test.txt', 'w')
    # f.write(str(a))
    # f.write('test2\n')
    a=a[::k,::k]
    # f.write(str(a))
    # f.write('test3\n')
    # f.write('test3\n')
    # print(a)
    img=Image.fromarray(a)
    # print(len(b),' ',len(b[0]),' ',len(b[0][0]),' ',time.time()-starttime)
    # print(a)
    # print(b)
    # img.save('image.png')
    img = np.array(img)
    return img

def SendDatThread(quenepipelineimg,message):
        quenepipelineimg.put(message)



class AnalyzeScreen:
    sct = mss()
    mon = {'top': 240, 'left': 235, 'width': 652, 'height': 480}
    object = 'neptune.png'
    start_time = time.time()
    fps = 0
    display_time = 1
    number = 0
    resme, resbullet, resenemies = 0, 0, 0
    threshold = 0.8
    # All the 6 methods for comparison in a list
    methods = ['cv.TM_CCOEFF', 'cv.TM_CCOEFF_NORMED', 'cv.TM_CCORR',
               'cv.TM_CCORR_NORMED', 'cv.TM_SQDIFF', 'cv.TM_SQDIFF_NORMED']
    template = 0
    bluedoggo = 0
    bluebullet = 0
    test=True
    bluedogoun=[]
    bluebulletun = []
    neptuneun=0
    quenepipeline=queue.Queue(maxsize=10)
    quenepipelineimg = queue.Queue(maxsize=10)
    # # event=threading.Event()
    img=0
    MadeScreenShot =0
    RecalculateSize =0
    PrepareScreenshot =0
    FindNeptune1 =0
    FindEnemies =0
    Senddatapack1=0
    Senddatapack2=0
    def __init__(self, quene,queneimg):
        self.template = self.EditFile(self.object)
        self.bluedoggo = self.EditFile('doggoblue.png')
        self.bluebullet = self.EditFile('bulletblue.png')
        self.w, self.h, self.z = self.template.shape
        self.quenepipeline = quene
        self.quenepipelineimg=queneimg
        print(quene.maxsize)
        print(queneimg.maxsize)
        self.test=True


    # def FindNeptune(self,img):
    #     value1,value2=int(len(self.template[0])/2),int(len(self.template)/2)
    #     value3,value4=int(len(img)),int(len(img[0]))
    #     d=4
    #     pixelinmidle=self.template[value1][value2]
    #     upperpixel=self.template[value1+d][value2+d]
    #     lowerpixel=self.template[value1-d][value2-d]
    #     for x in range(value3):
    #         for y in range(value4):
    #             if (img[x][y]==pixelinmidle).all():
    #                 # print(f'test1 {pixelinmidle} on {y} {x} comapre to {img[x][y]}')
    #                 cv.rectangle(img, (y,x), (y , x ), 255, 2)
    #                 if(value3-value2>x and value4-value1>y and (img[x+d][y+d]==upperpixel).all()) and (x+value2>=0 and y+value1>=0 and (img[x-d][y-d]==lowerpixel).all()):
    #                     # print(f'test2 {pixelinmidle} on {y} {x} comapre to {img[x][y]} +\n {upperpixel} on {y+3} {x+3} comapre to {img[x+3][y+3]} +\n {lowerpixel} on {y-3} {x-3} comapre to {img[x-3][y-3]}')
    #                     cv.rectangle(img, (y-value2,x-value1), (y+value2,x+value1), 255, 2)
    #                     return (y-value1,x-value2)
    #     return (-1,-1)

    def FindObject(self, object,):
        starttime = time.time()
        screenshot = self.sct.grab(self.mon)
        # img = Image.frombytes('RGB', (screenshot.width, screenshot.height), screenshot.rgb)
        img = np.array(screenshot)
        self.MadeScreenShot=time.time() - starttime
        starttime = time.time()
        img=RecalculateSizeSamller(img)
        self.RecalculateSize= time.time() - starttime
        starttime = time.time()
        img.astype(np.float32)
        # img = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
        img = cv.cvtColor(img, cv.COLOR_RGBA2RGB)
        self.PrepareScreenshot=time.time() - starttime
        starttime = time.time()

        method = eval(self.methods[self.number])
        self.resme = cv.matchTemplate(img, self.template, method)
        min_val, max_val, min_loc, max_loc = cv.minMaxLoc(self.resme)
        # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
        if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
            top_left = min_loc
        else:
            top_left = max_loc
        bottom_right = (top_left[0] + self.w, top_left[1] + self.h)
        cv.rectangle(img, top_left, bottom_right, 255, 2)
        self.neptuneun = (top_left, bottom_right)
        self.FindNeptune1= time.time() - starttime
        # starttime = time.time()
        # # Apply template Matching
        # neptunepos=self.FindNeptune(img)
        # if not neptunepos[0]==-1:
        #     self.neptuneun = (neptunepos, (neptunepos[0] + self.w, neptunepos[1] + self.h))
        # else:
        #     self.neptuneun=((0,0),(0,0))
        # print('FindNeptune: ', time.time() - starttime)
        starttime=time.time()
        # find enemies and generate rectangle around them
        bluedogoun = []
        bluebulletun = []
        self.resbullet = cv.matchTemplate(img, self.bluedoggo, cv.TM_CCOEFF_NORMED)
        we, he, ze = self.bluedoggo.shape
        loc = np.where(self.resbullet >= self.threshold)
        for pt in zip(*loc[::-1]):
            cv.rectangle(img, pt, (pt[0] + we, pt[1] + he), (0, 0, 255), 2)
            self.bluebulletun.append(pt)
        # find enemy bullets and generate rectangle around them
        self.resenemies = cv.matchTemplate(img, self.bluebullet, cv.TM_CCOEFF_NORMED)
        wb, hb, zb = self.bluebullet.shape
        loc = np.where(self.resenemies >= self.threshold)
        for pt in zip(*loc[::-1]):
            cv.rectangle(img, pt, (pt[0] + wb, pt[1] + hb), (0, 255, 128), 2)
            self.bluedogoun.append(pt)
        # print('test')
        self.img=img
        self.test=False
        mss().close()
        self.FindEnemies= time.time() - starttime

    def EditFile(self, name):
        plate = cv.imread(name, cv.IMREAD_UNCHANGED)
        plate = RecalculateSizeSamller(plate)
        plate = np.array(plate)
        plate.astype(np.float32)
        # plate = cv.cvtColor(plate, cv.COLOR_RGB2GRAY)
        plate = cv.cvtColor(plate, cv.COLOR_RGBA2RGB)
        return plate

    def LoopOfFinding(self):
            while True:
                self.fps = self.fps + 1
                if time.time() - self.start_time >= self.display_time:
                    self.start_time = time.time()
                    # print(self.fps)
                    # print('MadeScreenShot: ', self.MadeScreenShot)
                    # print('Recalculatesize: ', self.RecalculateSize)
                    # print('PrepareScreenshot: ', self.PrepareScreenshot)
                    # print('FindNeptune1: ', self.FindNeptune1)
                    # print('Time for calculating enemypos: ', self.FindEnemies)
                    # print('Senddatapack 1: ', self.Senddatapack1)
                    # print('Senddatapack 2: ', self.Senddatapack2)
                    print(f'Fps: {self.fps}, Prepare Img: {self.MadeScreenShot+self.RecalculateSize+self.PrepareScreenshot}'
                          f'Locate: {self.FindNeptune1+self.FindEnemies} Send: {self.Senddatapack1+self.Senddatapack2}')
                    self.fps = 0
                if keyboard.is_pressed("n"):
                    self.number = self.number + 1
                    # print("test ", self.number)
                    if len(self.methods) <= self.number:
                        self.number = 0
                        # print('test2')
                self.FindObject(object)
                self.SendDataToAnThread()
                if keyboard.is_pressed("q"):
                    cv.destroyAllWindows()
                    # print("test")
                    break



    def SendDataToAnThread(self):
        if self.quenepipeline.empty():
            starttime = time.time()
            message = (self.neptuneun, self.bluebulletun, self.bluedogoun)
            # print(message)
            x1 = threading.Thread(target=SendDatThread, args=(self.quenepipeline, message,), daemon=True)
            x1.start()

            self.Senddatapack1= time.time() - starttime

        if self.quenepipelineimg.empty():
            starttime = time.time()
            x1 = threading.Thread(target=SendDatThread, args=(self.quenepipelineimg, self.img,), daemon=True)
            x1.start()

            self.Senddatapack2= time.time() - starttime


class MovementControl:
    resme, resbullet, resenemies = 0, 0, 0
    resbulletun,resenemiesun=0,0
    threshold = 0.8
    # quenepipeline = queue.Queue(maxsize=10)
    # połnoc południe wschód zachód
    goto = [False, False, False, False]
    arrows = ['t', 'g', 'h', 'f']
    def __init__(self, quene):
        self.quenepipeline = quene
        print(quene.maxsize)
        self.start_time=time.time()
        self.fps=0
        self.display_time = 1

    def FireNonStop(self, kayoffire='x', sleeptime=0.01):
        keyboard.press(kayoffire)
        time.sleep(sleeptime)

    def LoadData(self):
        # print('test finded')
        message = self.quenepipeline.get()
        # print('test received')
        # print(message)

        self.resme, self.resbullet, self.resenemies = message[0], message[1], message[2]
        # loc = np.where(self.resbullet >= self.threshold)
        # self.resbulletun=zip(*loc[::-1])
        # self.resenemiesun = np.where(self.resenemies >= self.threshold)
        # print('restest1')
        # print(self.resenemiesun)
        # print('restest2')
        # self.resenemiesun=zip(*loc[::-1])
        # print(self.resenemiesun)

    def AnalyzeposMove(self):
        self.goto = [False, False, False, False]
        # min_val, max_val, min_loc, max_loc = cv.minMaxLoc(self.resme)
        # print(self.resme)
        # print(min_val,' ', max_val,' ', min_loc,' ', max_loc)
        ourPosition=self.resme[0]
        mindistance=999
        enemypos=[0,0]
        for pt in self.resenemies:
            # print('restest3')
            if not pt==[] and abs(ourPosition[1]-pt[1])<mindistance:
                # print('test2 ', pt[1])
                enemypos=pt
                mindistance=abs(ourPosition[1]-pt[1])
        # print('test3 enemypos',enemypos,' ourPosition ',ourPosition,' mindistance ',mindistance)
        if enemypos[1]-ourPosition[1]>0:
            self.goto[1]=True
        elif enemypos[1]-ourPosition[1]<0 and not enemypos==[0,0]:
            self.goto[0]=True

    def MoveOnAxis(self):
        for x in range(3):
            if self.goto[x]:
                keyboard.press(self.arrows[x])
                # print('test goto ',x)


    def LoopOfControl(self,loop=True, stopkay='p'):
        while loop:
            self.fps = self.fps + 1
            if time.time() - self.start_time >= self.display_time:
                self.start_time = time.time()
                print(self.fps)
                self.fps=0
            self.FireNonStop()
            self.MoveOnAxis()
            if not self.quenepipeline.empty():
                self.LoadData()
                if not self.resme==[]:
                    self.AnalyzeposMove()
            if keyboard.is_pressed(stopkay):
                break

# x=AnalyzeScreen()
# x.LoopOfFinding()


# FindObject(object)
