# try eexcept

def div(x, y):
    try:
        return x / y
    except ZeroDivisionError:
        print("error")


print(div(3, 0))

# uzytkowni kdaje z klawaiatury wartosc (liczba calkowita), do momentu podania liczby całkowitej
'''
while True:
    try:
        #test = int(input('Podaj Liczbe całkowita'))
        test = input("podaj liczbe calkowita\n")
        if float(test) % 1 == 0:
            print(f'{test} jest liczbą całkowitą')
            break
    except :
        print("error")
'''
# napisz petle while ktora bedzie pobirala od uzytkownika wartosc,
# w przypadku bledu wyswietli komunikat o tresci:
# podaj liczbe
# nie wolno dzielic przez 0
# wyswietl uzytkownikowi wynik dzielenia dwoch liczb podanych z plawiatury

while True:
    try:
        num1 = float(input('Podaj dzielną\n'))
        num2 = float(input('Podaj dzielnik\n'))
        print(num1 / num2)
    # break
    except ZeroDivisionError:
        print("Nie wolno dzielic przez 0")
    except:
        print("Dana nie jest liczbą")


