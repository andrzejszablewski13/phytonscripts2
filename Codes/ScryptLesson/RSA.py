import math


def mod(m, n):
    return m - n * math.floor(m / n)


def PowerMod(x, p, n):
    a = 1
    m = p
    t = x

    while m > 0:
        k = math.floor(m / 2)
        r = m - 2 * k
        if r == 1:
            a = mod(a * t, n)
        t = mod(t * t, n)
        m = k
    return a


def CheckCode(e, d, N, Msg):
    form = {1: PowerMod(Msg, e, N), 2: PowerMod(PowerMod(Msg, e, N), d, N)}
    return form


def fme(a, k, n):
    """
    modular exponentiation
    a ** k mod n
    """
    b = bin(k)[2:]  # list of bits
    m = len(b)
    r = 1  # result
    x = a % n
    for i in range(m - 1, -1, -1):
        if b[i] == '1':
            r = r * x % n
        x **= 2
        x %= n
        print(f'{b[i]}. s={r} x={x}')
    return r


print(CheckCode(149, 317, 8051, 249))
print(fme(249,149,8051))
print(fme(7885,317,8051))
