def Potegowanie(a, b):
    if b == 0:
        return 1
    elif b > 0:
        return a * Potegowanie(a, b - 1)
    else:
        return 1 / Potegowanie(a, b * -1)


print(Potegowanie(5, 3))
print(Potegowanie(5, -2))


def Fibonaci(n):
    if n == 1 or n == 2:
        return 1
    else:
        return Fibonaci(n - 1) + Fibonaci(n - 2)


print(Fibonaci(15))
print(Fibonaci(25))



def Fibonaci2(n):
    a, b = 0, 1
    for i in range(1, n):
        c=a
        a=b
        b=c+b
    return b


print(Fibonaci2(15))
import time
start=time.process_time()
print(Fibonaci2(35))
stop=time.process_time()
print(f'Czas wykonanaia ciągu fib iteracyjnie wynosi {stop-start}')
start=time.process_time()
print(Fibonaci(35))
stop=time.process_time()
print(f'Czas wykonanaia ciągu fib rekurencyjnie wynosi {stop-start}')