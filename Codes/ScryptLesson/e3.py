#in python all float are double (64bit)
a = float(input("Podaj liczbę a: "))
b = float(input("Podaj liczbę b: "))
c = float(input("Podaj liczbę c: "))
if c > 0:
    print("Wartość wyr. wynosi: " + str(a ** 2 + b))
elif c < 0:
    print("Wartość wyr. wynosi: " + str(a ** 2 - b))
else:
    if a == b:
        print("Nie dziel przez 0")
    else:
        print("Wartość wyr. wynosi: " + str(1 / (a - b)))
