dictio = {'key1': 'value1', 'key2': 'value2'}

worker = {
    "name": "Jan",
    "surname": "Nowak",
    "city": "Poznań",
    "age": 20,
    "namesChildren": ["Tomasz", "Krystyna"],
    "namesParent": ["Anna", "Krystyna"]
}
print(worker)
print(worker["namesChildren"])
print('Dziecko1: ' + str(worker["namesChildren"][0]))
print('Dziecko2: ' + str(worker["namesChildren"][1]))
worker["height"]=180
worker["weight"]=80
print(worker['height'])

key='age'
if key in worker:
    del worker[key]
    print(f'Klucz o nazwie {key} został usunięty')
else:
    print(f'Klucz o nazwie {key} nie został znaleziony')
print(worker.values())
print(worker.items())

for x in worker.values():
    print(x,end=" ")
print()
'''
wyswietl za pomoca for wartosci i klucze ze slownika worker w formacie:
klucz:wartosc
wykorzystaj w petli for funkcje print do wyswietlenia danych
'''
for x in worker.keys():
    print(str(x)+" "+str(worker[x]))
print()

for key, value in worker.items():
    print(f'{key}:{value}')
print()