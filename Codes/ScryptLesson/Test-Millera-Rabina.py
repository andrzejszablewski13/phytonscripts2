import random
import math

def fme(a, k, n):
    """
    modular exponentiation
    a ** k mod n
    """
    b = bin(k)[2:]  # list of bits
    m = len(b)
    r = 1  # result
    x = a % n
    for i in range(m - 1, -1, -1):
        if b[i] == '1':
            r = r * x % n
        x **= 2
        x %= n
        if b[i] =='1':
            print(f'Bit:{b[i]}. s=s*x%n={r} x=x^2%n={x}')
        else:
            print(f'Bit:{b[i]}. s=s={r} x=x^2%n={x}')
    return r

def FindT(n):
    t=(n - 1) / pow(2, 8)
    t=int(math.ceil(t))
    if t%2==0 and t!=0:
        t=t-1
    return t


def MillerRabbit(n):
    t = FindT(n)
    b = random.choice(range(2, n - 1))
    i = 0
    print(f'n={n}; t=(n-1)/2^8={t}; b={b}; numer pętli i={i}')
    c = 2
    while c != -1 and c != 1:
        if i == 9:
            break
        c = fme(b, t*pow(2,i), n)
        i=i+1
        print(f'tymczasowy wynik b^t^2*i mod n={c}. Numer następnej pętli{i}')

    if c==-1:
        print("Liczba pierwsza (raczej)")
    else:
        print("Na pewno nie pierwsza")

while True:
    dane=int(input("Podaj wartość n lub podaj -1 by zmaknąć\n"))
    if dane<0:
        break
    MillerRabbit(dane)
